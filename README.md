## Proyecto de prueba:

En este proyecto de prueba de maquetación se ha realizado siguiendo BEM, con una estructura
y organización diferentes a la de Jordi, sin utilizar ningún framework de estilos.

Aún así, hemos trabajado conjuntamente en pair programming en algunos puntos haciendo cada 
uno su propia barra de navegación.

Hemos querido hacer dos proyectos diferentes para que así veais dos maneras de trabajar
como también nuestra capacidad de adaptarnos e improvisar según lo demandado.

## Estructura del proyecto:

- src/assets -> Imágenes del proyecto
- src/components -> Componentes del proyecto
  - src/components/NavBar.vue -> Componente creado siguiendo la estructura BEM
  - src/components/NavBar.scss -> Componente de estilos creado siguiendo la estructura BEM

## Project setup
La versión de node (NVM) utilizada es v16.15.1, que es la última versión LTS. Ya está incluido en el fichero .nvmrc.
  ## Tutorial de Ejecución:

1. Instalar dependencias:
```
yarn install
```

2. Ejecutar el proyecto:
```
yarn serve
```

3. Visualizar el proyecto en el navegador:
```
  Local:   http://localhost:8080/ 
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

About [Yarn Reference](https://yarnpkg.com/).
